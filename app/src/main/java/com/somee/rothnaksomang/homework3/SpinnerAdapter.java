package com.somee.rothnaksomang.homework3;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class SpinnerAdapter extends ArrayAdapter<ItemData> {
    private Integer groupId;
    private Activity context;
    private ArrayList<ItemData> list;
    LayoutInflater inflater;

    public SpinnerAdapter(Activity context,int groupId,int id,ArrayList<ItemData> list){
        super(context,id,list);
        this.list=list;
        inflater=(LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        this.groupId=groupId;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        View itemView=inflater.inflate(groupId,parent,false);
        ImageView imageView=itemView.findViewById(R.id.ivCountryFlag);
        imageView.setImageResource(list.get(position).getPictureId());

        TextView textView=itemView.findViewById(R.id.tvCountryName);
        textView.setText(list.get(position).getText());
        return itemView;
    }

    public View getDropDownView(int position,View convertView,ViewGroup parent){
        return getView(position,convertView,parent);
    }

}
