package com.somee.rothnaksomang.homework3;

import android.os.Parcel;
import android.os.Parcelable;

public class Website implements Parcelable {
    private String title;
    private String urlType;
    private String url;
    private String email;
    private ItemData country;
    private String imgWebsite;


    public Website(String title, String urlType, String url, String email, ItemData country, String imgWebsite) {
        this.title = title;
        this.urlType = urlType;
        this.url = url;
        this.email = email;
        this.country = country;
        this.imgWebsite = imgWebsite;
    }


    protected Website(Parcel in) {
        title = in.readString();
        urlType = in.readString();
        url = in.readString();
        email = in.readString();
        country = in.readParcelable(ItemData.class.getClassLoader());
        imgWebsite = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(urlType);
        dest.writeString(url);
        dest.writeString(email);
        dest.writeParcelable(country, flags);
        dest.writeString(imgWebsite);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Website> CREATOR = new Creator<Website>() {
        @Override
        public Website createFromParcel(Parcel in) {
            return new Website(in);
        }

        @Override
        public Website[] newArray(int size) {
            return new Website[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public String getUrlType() {
        return urlType;
    }

    public String getUrl() {
        return url;
    }

    public String getEmail() {
        return email;
    }

    public ItemData getCountry() {
        return country;
    }

    public String getImgWebsite() {
        return imgWebsite;
    }
}
