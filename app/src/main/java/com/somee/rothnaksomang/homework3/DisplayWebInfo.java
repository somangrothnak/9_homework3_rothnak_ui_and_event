package com.somee.rothnaksomang.homework3;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class DisplayWebInfo extends AppCompatActivity {
    TextView tvTitle,tvWebType,tvURL,tvEmail,tvCountry;
    ImageView imgProfileWebsite_Display,ivFlagImage_Display;
    Button btnBack;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_web_info);


        tvTitle=findViewById(R.id.tvTitle_Display);
        tvWebType=findViewById(R.id.tvWebType_Display);
        tvURL=findViewById(R.id.tvURL_Display);
        tvEmail=findViewById(R.id.tvEmail_Display);
        tvCountry=findViewById(R.id.tvCountry_Display);
        imgProfileWebsite_Display=findViewById(R.id.imgProfileWebsite_Display);
        ivFlagImage_Display=findViewById(R.id.ivCountryFlag);
        btnBack=findViewById(R.id.btnBack);

        Intent i=getIntent();
        Website website=i.getParcelableExtra("message");

        String title=website.getTitle();
        String webType=website.getUrlType();
        String url=website.getUrl();
        String email=website.getEmail();
        ItemData country=website.getCountry();
        String image=website.getImgWebsite();
        Uri imageUri=Uri.parse(image);

        tvTitle.setText(title);
        tvWebType.setText(webType);
        tvURL.setText(url);
        tvEmail.setText(email);
        tvCountry.setText(country.getText());
        ivFlagImage_Display.setImageResource(country.getPictureId());
        imgProfileWebsite_Display.setImageURI(imageUri);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getApplicationContext(),MainActivity.class);
                startActivity(i);
            }
        });

    }
}
