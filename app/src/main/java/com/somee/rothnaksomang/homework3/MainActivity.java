package com.somee.rothnaksomang.homework3;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity {

    private Button btnChooseImage,btnSave;
    private CircleImageView imgProfileWebsite;
    private EditText etWebsite,etEmail,etTitleWebsite;
    private Spinner spnCountry;
    private RadioGroup rdgWebType;
    private RadioButton rdbWebsite,rdbFacebook;
    private static final String IMAGE_DIRECTORY="/demonuts";
    private int GALLERY=1,CAMERA=2;
    Uri imgWebsite;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setValueToSpinner();

//        referent variable to xml file
        btnChooseImage=findViewById(R.id.btnChooseImage);
        imgProfileWebsite=findViewById(R.id.imgProfileWebsite);
        btnSave=findViewById(R.id.btnSave);
        etTitleWebsite=findViewById(R.id.etTitleWebsite);
        etEmail=findViewById(R.id.etEmail);
        etWebsite=findViewById(R.id.etWebsite);
        rdgWebType=findViewById(R.id.rdgWebType);
        rdbWebsite=findViewById(R.id.rdbWebsite);
        rdbFacebook=findViewById(R.id.rdbFacebook);
        spnCountry=findViewById(R.id.spnCountry);
        rdbWebsite.setChecked(true);

        btnChooseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //call function to show Dialog for choose image
                showPictureDialog();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveData();

            }
        });
    }

    public void saveData(){
//        declare variable
        Website website;
        String title = null;
        String rdgType;
        String url;
        String email;
        ItemData country;
//        set Value to variable
        title=etTitleWebsite.getText().toString();
//        referent variable to xml file
        if(rdbWebsite.isChecked()==true){
            rdgType=rdbWebsite.getText().toString();
        }else{
            rdgType=rdbFacebook.getText().toString();
        }
        url=etWebsite.getText().toString();
        email=etEmail.getText().toString();
        country= (ItemData) spnCountry.getSelectedItem();

        if(title.equals("")){
            etTitleWebsite.requestFocus();
            Toast.makeText(getApplicationContext(),"Title null",Toast.LENGTH_SHORT).show();
        }else if(url.equals("")){
            etWebsite.requestFocus();
            Toast.makeText(getApplicationContext(),"URL null",Toast.LENGTH_SHORT).show();

        }else if(email.equals("")){
            etEmail.requestFocus();
            Toast.makeText(getApplicationContext(),"Email null",Toast.LENGTH_SHORT).show();

        }else if(imgWebsite==null){
            Toast.makeText(getApplicationContext(),"Image null",Toast.LENGTH_SHORT).show();
        }else{
            website=new Website(title,rdgType,url,email,country,imgWebsite.toString());
            Intent i=new Intent(getApplicationContext(),DisplayWebInfo.class);
            i.putExtra("message",website);
            startActivity(i);
            etTitleWebsite.setText("");
            etWebsite.setText("");
            etEmail.setText("");
        }
    }

    public void showPictureDialog(){
        AlertDialog.Builder pictureDialog=new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems={
                "Select Photo from Gallery",
                "Capture Photo from Camera"
        };
        pictureDialog.setItems(pictureDialogItems, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case 0:
                        choosePhotoFromGallery();
                        break;
                    case 1:
                        takePhotoFromCamera();
                        break;

                }
            }
        });
        pictureDialog.show();
    }

    //pick Photo from Gallery
    public void  choosePhotoFromGallery(){
        Intent galleryIntent=new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent,GALLERY);
    }

    //pick Photo from Camera by Take
    public void takePhotoFromCamera(){
        Intent intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent,CAMERA);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==this.RESULT_CANCELED){
            return;
        }
        if(requestCode == GALLERY){
            if(data !=null){
                Uri contentURI=data.getData();
                imgWebsite=data.getData();
                Toast.makeText(MainActivity.this,contentURI+ "",Toast.LENGTH_SHORT).show();
                try{
                    Bitmap bitmap=MediaStore.Images.Media.getBitmap(this.getContentResolver(),contentURI);

                    String path=saveImage(bitmap);
                    Toast.makeText(MainActivity.this,"Image Saved!",Toast.LENGTH_SHORT).show();
                    imgProfileWebsite.setImageBitmap(bitmap);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Toast.makeText(MainActivity.this,"False!",Toast.LENGTH_SHORT).show();

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(MainActivity.this,"False!",Toast.LENGTH_SHORT).show();
                }
            }
        }else if(requestCode == CAMERA){
            Bitmap thumbnai= (Bitmap) data.getExtras().get("data");
            imgProfileWebsite.setImageBitmap(thumbnai);
            saveImage(thumbnai);
            Toast.makeText(MainActivity.this,"Image Saved!",Toast.LENGTH_SHORT).show();

        }

    }

    public String saveImage(Bitmap myBitmap){
        ByteArrayOutputStream byteArrayOutputStream=new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
        File wallpaperDirectory=new File(Environment.getExternalStorageDirectory()+IMAGE_DIRECTORY);

        if(wallpaperDirectory.exists()){
            wallpaperDirectory.mkdirs();
        }
        try{
            File f=new File(wallpaperDirectory,Calendar.getInstance().getTimeInMillis()+".jpg");
            f.createNewFile();
            FileOutputStream fo=new FileOutputStream(f);
            fo.write(byteArrayOutputStream.toByteArray());
            MediaScannerConnection.scanFile(this,new String[]{f.getPath()},new String[]{"image/jpeg"},null);

            fo.close();
            Log.d("TAG", "File Saved");

            return f.getAbsolutePath();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    //set default Value to Spinner
    //the value have Image and Name of Country
    public void setValueToSpinner(){
        ArrayList<ItemData> list=new ArrayList<>();
        list.add(new ItemData("Cambodia",R.drawable.ic_cambodia));
        list.add(new ItemData("Australia",R.drawable.ic_australia));
        list.add(new ItemData("Brazil",R.drawable.ic_brazil));
        list.add(new ItemData("China",R.drawable.ic_china));
        list.add(new ItemData("France",R.drawable.ic_france));
        list.add(new ItemData("Germany",R.drawable.ic_germany));
        list.add(new ItemData("Japan",R.drawable.ic_japan));
        list.add(new ItemData("Laos",R.drawable.ic_laos));
        list.add(new ItemData("Netherlands",R.drawable.ic_netherlands));
        list.add(new ItemData("South Korea",R.drawable.ic_south_korea));
        list.add(new ItemData("United Kingdom",R.drawable.ic_united_kingdom));
        list.add(new ItemData("United States",R.drawable.ic_united_states));
        list.add(new ItemData("Vietnam",R.drawable.ic_vietnam));

        Spinner spinner=findViewById(R.id.spnCountry);
        SpinnerAdapter spinnerAdapter=new SpinnerAdapter(this,R.layout.activiry_spinner,R.id.tvCountryName,list);
        spinner.setAdapter(spinnerAdapter);
    }


}
